---
title: "Frequently Asked Questions"
date: 2020-03-01T16:09:45-04:00
#headline: "The Community for Open Innovation and Collaboration"
#tagline: "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
hide_page_title: true
#hide_sidebar: true
#hide_breadcrumb: true
#show_featured_story: true
layout: "single"
#links: [[href: "/projects/", text: "Projects"],[href: "/org/workinggroups/", text: "Working Group"],[href: "/membership/", text: "Members"],[href: "/org/value", text: "Business Value"]]
#container: "container-fluid"

page_css_file: "custom-theme.css"
---

# Chat Service Frequently Asked Questions

From troubleshooting common issues to learning how to use various chat features, we've got you covered. If you can't find what you're looking for, please don't hesitate to reach out to our support team for further assistance.

You can access our support service by asking on the [#eclipsefdn.chat-support:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipsefdn.chat-support:matrix.eclipse.org) room, or by opening an issue in our [helpdesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new?issue[title]=%5BChat%20service%5D%20Create%20room%20and%20space%20for%20%3CProject%3E&issuable_template=chat-service).

Check our [support documentation page](https://chat.eclipse.org/docs/support) for more details!


<!--
{% raw %}
-->

{{< toc >}}

<!-- 
{% endraw %}
-->

## **Request for room/space**

### *Q: How can I create a specific room/space?*

A: You can create your own `Merge Request` on the [Chat Service Provisioner](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/chat-service-provisioner) on this [project.yaml](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/chat-service-provisioner/-/blob/main/project.yaml) file, by following the [configuration specification](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/chat-service-sync/-/blob/main/readme.md#project-roomspace-definition).

It is based on the `chat-service-sync` tool, which allows the creation of rooms/spaces as code, enabling their organization and the assignment of permissions to users. By default, the project lead will have a specific moderator role, enabling them to send invitations, change room topics, etc. However, there are limitations for room name, room avatar, room alias (main address), kicking and banning users, and deleting messages.

NOTE: Avatars are not yet managed by the `as-code` approach. Please provide the avatar media directly in the Merge Request or helpdesk issue. It will be added manually for now.

A: Or you can create a request directly via the helpdesk by using an issue template. Here are the steps:
* Create the issue in the [helpdesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new?issue[title]=%5BChat%20service%5D%20Create%20room%20and%20space%20for%20%3CProject%3E&issuable_template=chat-service) 
* Change Title issue by defining `<Project>`: `[Chat service] Create room and space for <Project>` 
* Choose template `chat-service` or copy/paste the template: https://chat.eclipse.org/docs/request
* And fill in all the necessary information

IMPORTANT: Project Lead must approve the PR or the ticket.
_________________

## **Room/space settings**

### *Q: How can I change some room/space settings like name, alias, topic, or avatar?*

A: The ability to modify room settings on Eclipse is restricted to Project Leads only. Only Project Leads have the permission to make configuration changes and it's limited only to changing the room `topic`. For all other room/space settings such as `name`, `alias` or `avatar`, users must fill a helpdesk issue. Only admins can change these settings.  

> IMPORTANT: Only project leads using an eclipse account and not a federated account can obtain moderation permissions.

_________________

## **Using accounts from federated servers**

### *Q: Can I participate in a room with my account from matrix.org or any other matrix server?*

A: Yes, existing Matrix users can participate in our chat rooms and spaces with their current account from any other Matrix server via federation and with any client of their choice.

By using our chat service, Eclipse members can easily authenticate using their Eclipse Foundation account. We encourage all Eclipse members to use our chat service as their primary point of entry to our Matrix server, but we welcome all those who share our community passion for professional, collegial and respectful discussions about open source.

For those who don't have an Eclipse Foundation account, you can participate in our chat rooms and spaces by simply searching for the room or space you're interested in and joining as you would normally. 

_________________

## **Eclipse Foundation Policy Bot**

### *Q: What is the role of the Eclipse Foundation Policy Bot in the Matrix server of the foundation?*
A: The Eclipse Foundation Policy Bot is an appservice on the Matrix server designed to inform users who join a room or space about the different policies that apply to that room or space. This can include policies related to the code of conduct, privacy policy, or other guidelines specific to the room or space. The bot is intended to ensure that users are aware of the expectations and rules when participating in conversations.

### *Q: When I first logged in, why did I get multiple messages from this bot?*
A: The first time a user logs into the platform, the Eclipse Foundation's chat service automatically attaches spaces to the user, including `#eclipsefdn` and `#eclipse-projects`, which results in multiple join events on the platform. The appservice whose role is simply to send a notification message to all users joining a room or space, therefore has the consequence of multiple notifications upon first login. However, this should not happen again, unless the user joins a new room later that is not part of the auto-join rooms.

_________________

## **Phone number setup error**

### *Q: Why can't I setup my phone number?*

A: Currently our chat service instance is not configured to support SMS. However, it does support email notifications.

It's also worth noting that some Matrix clients may have built-in support for SMS or other mediums, even if the chat instance at the Eclipse Foundation does not. If you're using a third-party client, check the documentation or settings to see if SMS is supported.

_________________

## **Room/space creation error**

### *Q: Why does the web UI freeze and I am receiving a 403 error from the Matrix server when trying to create a room or space in the chat service?*

A: At this time creation of rooms and spaces is restricted.

### *Q: What should I do if I need to create a room or space?*

A: If your Eclipse project does not have a room or a space yet, you can request it by opening a [HelpDesk issue](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new).

For more information, see [How can I create a specific room/space?](https://chat.eclipse.org/docs/faq/#q-how-can-i-create-a-specific-roomspace)

### *Q: Can I still join existing rooms or spaces even if I cannot create new ones?*

A: Yes, you can still join existing rooms or spaces even if you cannot create new ones. However, some rooms or spaces you want to join may have additional restrictions on joining.

_________________

## **Moderation**

### *Q: Will users be given moderation rights on the chat service?*
A: Only project leads will have the ability to acquire moderation rights on rooms/spaces corresponding to their projects on the chat service. 

### *Q: Who will handle moderation on the chat platform?*
A: Moderation will be handled by admin but also by project leads in their respective project, they have the right to kick, ban and remove messages, ... 

### *Q: What actions will be taken against users who violate the code of conduct policy?*
A: See [Consequences of Unacceptable Behavior](https://www.eclipse.org/org/documents/communication-channel-guidelines/)

_________________

## **Reporting bad content**

### *Q: How can I report bad content on the chat service?*

A: To report bad content on Matrix, you can use the "Report" feature from your client. This will allow you to report a specific message or user for inappropriate behavior or content to administrators. Alternatively, the helpdesk can be used for this purpose.

### *Q: What kind of content can I report?*

A: You can report any content that violates the Matrix server's terms of service or code of conduct. This includes messages or files that contain hate speech, harassment, or other inappropriate content.

### *Q: What happens after I report bad content?*

A: After you report bad content, administrators will review the report and take appropriate action. This may include removing the content or taking action against the user who posted it.

### *Q: What should I do if I receive a warning or ban for bad content?*

A: If you receive a warning or ban for bad content, you should review the Matrix server's terms of service and code of conduct to understand why your behavior was deemed inappropriate. If you believe the warning or ban was a mistake, you can contact administrators.

### *Q: How can I avoid posting bad content on Matrix?*

A: The best way to avoid posting bad content is to familiarize yourself with the server's terms of service and code of conduct, and to use common sense and good judgment when posting messages or files. If you're not sure whether a message or file is appropriate, it's better to not post it.

_________________

## **Private room**

### *Q: Is it possible to create private rooms in the Eclipse Foundation's chat service?*
A: No, it is not possible to create private rooms in the Eclipse Foundation's chat service in accordance with Eclipse principles.

### *Q: Why is it not possible to create private rooms in the Eclipse Foundation's chat service?*
A: The Eclipse Foundation's commitment to openness and transparency means that private rooms are not consistent with its principles. All conversations in the chat service should be visible to all members of the community to ensure transparency and encourage collaboration.

_________________

## **Encrypted messages are not readable**

### *Q: Why am I seeing "Messages not readable" errors?*

A: You may see this error if you have received messages that were sent in an encrypted format, and your device or Element Web instance does not have the necessary encryption keys to decrypt and display the messages. To fix this, you can log in to your Matrix account on the device or instance where the messages were originally sent or contact the sender to request that they resend the messages in an unencrypted format.

### *Q: How can I prevent "Messages not readable" errors in the future?*

A: Ensure that your device or Element Web instance has the necessary encryption keys to decrypt messages sent in encrypted or "not readable" format. Additionally, you should ensure that your device or instance is up-to-date and running the latest version of Element Web to avoid any compatibility issues.

### *Q: Why can't I post an encrypted message to a room?*

A: The chat service hosted at the Eclipse Foundation only allows encrypted messages for 1:1 communication. All rooms are public and not encrypted.

_________________

## **Lost my encryption keys**

### *Q: What happens if I lose my encryption keys?*

A: Losing your encryption keys means you can't access your encrypted messages or history. 

### *Q: Can I recover my lost encryption keys?*

A: Only if you use secure backup otherwise Matrix doesn't have access to your keys, and they can't be reset. They are stored locally on each device for maximum security.

### *Q: What can I do if I lose my encryption keys?*

A: Inform contacts about the loss and consider restoring a device backup if available. Generating new keys if no backup, but you'll lose access to old encrypted messages.

### *Q: How can I prevent losing my encryption keys?*

A: Back up your keys securely, use trusted devices for Matrix, and use Secure Backup. (In Elementweb client: profile -> All settings -> Security and Privacy -> Secure Backup).

_________________

## **Private conversation in 1:1 recommendations**

### *Q: Can I invite others to join my direct conversation on the chat service platform?*
A: It is not recommended to invite or involve other individuals in direct conversations on the chat service platform. Doing so may violate the principles of the Eclipse Foundation in not participating in public channels.

### *Q: How can I ensure I'm complying with the principles of the Eclipse Foundation in my communications?*
A: To ensure compliance, it is recommended to familiarize yourself with the guidelines provided by the Eclipse Foundation. 
https://www.eclipse.org/projects/handbook/#preamble-principles

### *Q: What should I do if I want to involve multiple people in a discussion?*
A: If you wish to involve multiple individuals in a discussion, it is recommended to start chatting in projects/foundation public channels provided by the chat service platform. These avenues promote open collaboration while respecting the principles of the Eclipse Foundation.

_________________

## **Media storage restriction**

### *Q: What is the maximum media size that can be shared on the Chat service?*

A: The maximum media size that can be shared or uploaded as media, such as images or videos on the chat service is 100MB per upload with quotas of 50GB per user.

### *Q: Why are there media size restrictions in the Chat service?*

A: Media size restrictions are in place to prevent the overuse of server resources and to ensure that Matrix remains a fast and reliable communication platform for all users.

### *Q: What happens if I try to upload a media file that exceeds the size limit?*

A: If you try to upload a media file that exceeds the size limit, you may receive an error message or the upload may fail. 

### *Q: What should I do if I need to share a large media file on chat service but it exceeds the size limit?*

A:  You may want to consider using a third-party service to host the file and sharing the link in Matrix. Alternatively, you can contact support to see if there are any alternative solutions available.

_________________

## **Is our server on your federation blacklist?**

### *Q: How can I determine if my Matrix server is blacklisted?*

A: Currently we have no server on our federation blacklist. This cannot be the reason for your federation problem.
_________________

## **Matrix client support other than element-web hosted at Eclipse**

### *Q: Can the support team help with issues related to other Matrix clients?*

A: We regret to inform you that we cannot provide support for any issues that arise from using other Matrix clients.

_________________

## **Video/voice call**

### *Q: Can I make voice and video calls using Element Web on the Eclipse Foundation chat service?*

A: No, voice and video calls are currently not supported on the Eclipse Foundation chat service using Element Web.

### *Q: Will voice and video calls be supported in the future?*

A: The development team is continuously working to improve and enhance the features of the chat service. While we cannot guarantee that voice and video calls will be supported in the future, we will continue to listen to feedback from our community and prioritize features accordingly.

_________________

## **Communicating with other protocols(Slack, Whatsapp, etc)**

### *Q: Is the Eclipse Foundation chat service currently integrated with other platforms like WhatsApp or Slack?*

A: Yes and no, the Eclipse Foundation chat service can only connect to Slack channels. For other platforms, specific bridges need to be installed and managed. 

_________________

## **Can I use matrix bots?**

### *Q: Why doesn't the Eclipse Foundation currently implement matrix bots in its chat service?*

A: The Eclipse Foundation has not implemented any matrix bots in its chat service for a specific reason:
* Focus on core features: The Foundation is currently focused on improving the core features of its chat service, such as usability, performance, and security. This means that bots may not be a priority at this time.
_________________

## **What about Matrix 2.0?**

A: The matrix foundation announce recently [Matrix version 2.0](https://matrix.org/blog/2023/09/matrix-2-0/) while it is still in active development the Eclipse Foundation's chat service will not deploy this version in its current form and the service will not be usable with Element X or any compatible apps. However, we are constantly monitoring the progress of the project and will make the transition when the time is right.