---
title: Rooms/Spaces
date: 2020-03-01T16:09:45-04:00
#headline: The Community for Open Innovation and Collaboration
#tagline: The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation.
hide_page_title: true
#hide_sidebar: true
#hide_breadcrumb: true
#show_featured_story: true
layout: single
#links: [[href: /projects/, text: Projects],[href: /org/workinggroups/, text: Working Group],[href: /membership/, text: Members],[href: /org/value, text: Business Value]]
#container: container-fluid
---
# Chat Service Exploration: Rooms & Spaces
Discover Eclipse Foundation's Chat Service by exploring rooms and spaces. Engage with projects members (project leads, commiters, contributors, ...), participate in discussion, and foster innovation within our vibrant community. 

Whether it's your first time here or if you need assistance, take a look at our [getting started!](https://chat.eclipse.org/docs/getting-started/) guide and our comprehensive [FAQ](https://chat.eclipse.org/docs/faq) to answer any questions.

<!--
{% raw %}
-->

{{< toc >}}

<!-- 
{% endraw %}
-->
*last update: 03/09/2025*

_________________
## **Eclipse 4diac™ Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="3" >}} {{< badge prefix=people text="41" >}}<!--{% endraw %}-->
**[#eclipse-4diac:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-4diac:matrix.eclipse.org)**

Eclipse 4diac™ provides an open source infrastructure for distributed industrial process measurement and control systems based on the IEC 61499 standard.
### Eclipse 4diac FORTE Room <!--{% raw %}-->{{< badge prefix="people" text="35" >}}<!--{% endraw %}-->
**[#4diac-forte:matrix.eclipse.org](https://chat.eclipse.org/#/room/#4diac-forte:matrix.eclipse.org)**

Questions and discussions targeting 4diac FORTE.
### Eclipse 4diac IDE Room <!--{% raw %}-->{{< badge prefix="people" text="30" >}}<!--{% endraw %}-->
**[#4diac-ide:matrix.eclipse.org](https://chat.eclipse.org/#/room/#4diac-ide:matrix.eclipse.org)**

Questions and discussions targeting 4diac IDE.
### Eclipse 4diac Town Square Room <!--{% raw %}-->{{< badge prefix="people" text="21" >}}<!--{% endraw %}-->
**[#4diac-townsquare:matrix.eclipse.org](https://chat.eclipse.org/#/room/#4diac-townsquare:matrix.eclipse.org)**

A town square for the Eclipse 4diac project, the place for conversations that do not fit any dedicated Matrix room.
_________________
## **Eclipse Adoptium™ Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="8" >}} {{< badge prefix=people text="10" >}}<!--{% endraw %}-->
**[#adoptium:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium:matrix.eclipse.org)**

The mission of the Eclipse Adoptium Top-Level Project is to produce high-quality runtimes and associated technology for use within the Java ecosystem.  We achieve this through a set of Projects under the Adoptium PMC and a close working partnership with external projects, most notably OpenJDK for providing the Java SE runtime implementation.  Our goal is to meet the needs of both the Eclipse community and broader runtime users by providing a comprehensive set of technologies around runtimes for Java applications that operate alongside existing standards, infrastructures, and cloud platforms.The AdoptOpenJDK project was established in 2017 following years of discussions about the general lack of an open and reproducible build and test system for OpenJDK source across multiple platforms.  Since then it has grown to become a leading provider of high-quality OpenJDK-based binaries used by enterprises in embedded systems, desktops, traditional servers, modern cloud platforms, and large mainframes.  The Eclipse Adoptium project is the continuation of the original AdoptOpenJDK mission.
### Adoptium Build Room <!--{% raw %}-->{{< badge prefix="people" text="515" >}}<!--{% endraw %}-->
**[#adoptium-build:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-build:matrix.eclipse.org)**

Discussions related to the building of anything in the Adoptium machine farmNightly triage: https://docs.google.com/document/d/1vcZgHJeR8rW8U8OD23Uob7A1dbLrtkURZUkinUp7f_w/edit?usp=sharing
### Adoptium Containers Room <!--{% raw %}-->{{< badge prefix="people" text="31" >}}<!--{% endraw %}-->
**[#adoptium-containers:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-containers:matrix.eclipse.org)**

Topics relating to containers
### Adoptium General Room <!--{% raw %}-->{{< badge prefix="people" text="560" >}}<!--{% endraw %}-->
**[#adoptium-general:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-general:matrix.eclipse.org)**

https://github.com/adoptium/adoptium - starting point for build farm folkshttps://drive.google.com/drive/folders/1jjdXw3fAgRIeupb_bXCoOgFH9_iyo1ak - meeting agendas / minutes
### Adoptium Infrastructure Room <!--{% raw %}-->{{< badge prefix="people" text="49" >}}<!--{% endraw %}-->
**[#adoptium-infrastructure:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-infrastructure:matrix.eclipse.org)**

Discussion relating to the machines and services supporting OpenJDK build & test. Machine list @ https://github.com/adoptium/infrastructure/blob/master/ansible/inventory.yml
### Adoptium Installer Room <!--{% raw %}-->{{< badge prefix="people" text="33" >}}<!--{% endraw %}-->
**[#adoptium-installer:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-installer:matrix.eclipse.org)**

Topics relating to installer
### Adoptium Release Room <!--{% raw %}-->{{< badge prefix="people" text="72" >}}<!--{% endraw %}-->
**[#adoptium-release:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-release:matrix.eclipse.org)**

Topics relating to release: https://github.com/adoptium/temurin-build/blob/master/RELEASING.md
### Adoptium Secure Dev Room <!--{% raw %}-->{{< badge prefix="people" text="36" >}}<!--{% endraw %}-->
**[#adoptium-secure-dev:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-secure-dev:matrix.eclipse.org)**

Topics relating to Secure Dev: https://docs.google.com/document/d/1bxYCEbM4Wn2uLl_lgY67a6x5VBWH_ZGdLwG_yDByfT0/edit?pli=1#heading=h.bti4iq4qm4f0
### Adoptium Testing Aqavit Room <!--{% raw %}-->{{< badge prefix="people" text="512" >}}<!--{% endraw %}-->
**[#adoptium-testing-aqavit:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-testing-aqavit:matrix.eclipse.org)**

Topics relating to testing activities done as part of the activities under the AQAvit project
_________________
## **Eclipse Foundation Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="4" >}} {{< badge prefix=people text="650" >}}<!--{% endraw %}-->
**[#eclipsefdn:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipsefdn:matrix.eclipse.org)**

Open communication channel at Eclipse Foundation
### IP License compliance Room <!--{% raw %}-->{{< badge prefix="people" text="42" >}}<!--{% endraw %}-->
**[#eclipsefdn.ip-license-compliance:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipsefdn.ip-license-compliance:matrix.eclipse.org)**

Welcome to the IP license compliance room!
### IT Room <!--{% raw %}-->{{< badge prefix="people" text="41" >}}<!--{% endraw %}-->
**[#eclipsefdn.it:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipsefdn.it:matrix.eclipse.org)**

Welcome to the IT team room! This room is a space for the Eclipse Foundation IT staff to discuss, report, and troubleshoot technical issues related to our organization's infrastructure and systems. And don't forget to report issues via [Eclipse HelpDesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new)
### chat service support Room <!--{% raw %}-->{{< badge prefix="people" text="1081" >}}<!--{% endraw %}-->
**[#eclipsefdn.chat-support:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipsefdn.chat-support:matrix.eclipse.org)**

Welcome to the chat support room! This room is to provide help and assistance with our chat service. Feel free to ask questions, report issues, or share feedback. Full FAQ is available [here](https://chat.eclipse.org/docs/faq/)
### general Room <!--{% raw %}-->{{< badge prefix="people" text="1101" >}}<!--{% endraw %}-->
**[#eclipsefdn.general:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipsefdn.general:matrix.eclipse.org)**

Welcome to the general room! This room is a place for users to chat and discuss a wide range of topics about the chat service, the foundation, Eclipse projects, ECA, contribution, ...Feel free to introduce yourself, share your interests, and start ask questions. Full Documentation is available [here](https://chat.eclipse.org/docs/).Want new rooms/spaces, please follow intructions [here](https://chat.eclipse.org/docs/faq/#request-for-roomspace)
_________________
## **Eclipse IDE Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="3" >}} {{< badge prefix=people text="68" >}}<!--{% endraw %}-->
**[#eclipse-ide:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-ide:matrix.eclipse.org)**

This is the space of the Eclipse IDE Foundation's flagship "IDE" Community, it includes all Eclipse open source projects that participate in the Eclipse IDE simultaneous release.
### Eclipse-IDE General Room <!--{% raw %}-->{{< badge prefix="people" text="65" >}}<!--{% endraw %}-->
**[#eclipse-ide-general:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-ide-general:matrix.eclipse.org)**

General Community discussions on IDE and all Eclipse open source projects that participate in the Eclipse IDE simultaneous release.[Meeting calendars](https://calendar.google.com/calendar/u/0/embed?src=c_cpu7j4g65ejc9q5uiq641f9k5c@group.calendar.google.com)
### Eclipse-IDE Newcomers Room <!--{% raw %}-->{{< badge prefix="people" text="45" >}}<!--{% endraw %}-->
**[#eclipse-ide-newcomers:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-ide-newcomers:matrix.eclipse.org)**

Eclipse IDE Newcomers Room: feel free to ask questions, seek guidance, and connect with other newcomers and experienced community members.
### Eclipse-IDE SWT Initiative 31 Room <!--{% raw %}-->{{< badge prefix="people" text="28" >}}<!--{% endraw %}-->
**[#swt-initiative-31:matrix.eclipse.org](https://chat.eclipse.org/#/room/#swt-initiative-31:matrix.eclipse.org)**

This is the room to discuss 'Initiative 31' (https://github.com/swt-initiative31), a project dedicated to exploring options for modernizing the Eclipse IDE Platform and SWT, aiming to make them more future-proof.
_________________
## **Eclipse Modeling builds Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="3" >}} {{< badge prefix=people text="2" >}}<!--{% endraw %}-->
**[#eclipse-modeling-build:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-modeling-build:matrix.eclipse.org)**

Space related to all modeling project build rooms
### Eclipse Modeling Workflow Engine Builds Room <!--{% raw %}-->{{< badge prefix="people" text="3" >}}<!--{% endraw %}-->
**[#modeling-emf-mwe-builds:matrix.eclipse.org](https://chat.eclipse.org/#/room/#modeling-emf-mwe-builds:matrix.eclipse.org)**

Eclipse Modeling Workflow Engine Builds
### Eclipse Xpect Builds Room <!--{% raw %}-->{{< badge prefix="people" text="3" >}}<!--{% endraw %}-->
**[#modeling-xpect-build:matrix.eclipse.org](https://chat.eclipse.org/#/room/#modeling-xpect-build:matrix.eclipse.org)**

Notification Channel for Eclipse Xpect™ Builds
### Xtext Builds Room <!--{% raw %}-->{{< badge prefix="people" text="5" >}}<!--{% endraw %}-->
**[#xtext-releng:matrix.eclipse.org](https://chat.eclipse.org/#/room/#xtext-releng:matrix.eclipse.org)**

Notification Channel for Xtext Build Results
_________________
## **Eclipse Projects Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="49" >}} {{< badge prefix=people text="652" >}}<!--{% endraw %}-->
**[#eclipse-projects:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-projects:matrix.eclipse.org)**

Welcome to the Eclipse projects space! This space is dedicated to discussions and collaboration around projects developed under the Eclipse Foundation.We encourage active participation and constructive discussions. Please feel free to share your ideas, ask for advice, provide feedback, report issues... This space is a community hub for Eclipse-related projects, and we welcome all perspectives and contributions.To ensure a respectful environment, please be aware of our [privacy policy](https://www.eclipse.org/legal/privacy.php) and follow our [code of conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php) and `guidelines`. Full documentation can be found [here](https://chat.eclipse.org/docs/) and [FAQ](https://chat.eclipse.org/docs/faq)
### Adoptium Build Room <!--{% raw %}-->{{< badge prefix="people" text="515" >}}<!--{% endraw %}-->
**[#adoptium-build:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-build:matrix.eclipse.org)**

Discussions related to the building of anything in the Adoptium machine farmNightly triage: https://docs.google.com/document/d/1vcZgHJeR8rW8U8OD23Uob7A1dbLrtkURZUkinUp7f_w/edit?usp=sharing
### Adoptium Containers Room <!--{% raw %}-->{{< badge prefix="people" text="31" >}}<!--{% endraw %}-->
**[#adoptium-containers:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-containers:matrix.eclipse.org)**

Topics relating to containers
### Adoptium General Room <!--{% raw %}-->{{< badge prefix="people" text="560" >}}<!--{% endraw %}-->
**[#adoptium-general:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-general:matrix.eclipse.org)**

https://github.com/adoptium/adoptium - starting point for build farm folkshttps://drive.google.com/drive/folders/1jjdXw3fAgRIeupb_bXCoOgFH9_iyo1ak - meeting agendas / minutes
### Adoptium Infrastructure Room <!--{% raw %}-->{{< badge prefix="people" text="49" >}}<!--{% endraw %}-->
**[#adoptium-infrastructure:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-infrastructure:matrix.eclipse.org)**

Discussion relating to the machines and services supporting OpenJDK build & test. Machine list @ https://github.com/adoptium/infrastructure/blob/master/ansible/inventory.yml
### Adoptium Installer Room <!--{% raw %}-->{{< badge prefix="people" text="33" >}}<!--{% endraw %}-->
**[#adoptium-installer:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-installer:matrix.eclipse.org)**

Topics relating to installer
### Adoptium Release Room <!--{% raw %}-->{{< badge prefix="people" text="72" >}}<!--{% endraw %}-->
**[#adoptium-release:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-release:matrix.eclipse.org)**

Topics relating to release: https://github.com/adoptium/temurin-build/blob/master/RELEASING.md
### Adoptium Secure Dev Room <!--{% raw %}-->{{< badge prefix="people" text="36" >}}<!--{% endraw %}-->
**[#adoptium-secure-dev:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-secure-dev:matrix.eclipse.org)**

Topics relating to Secure Dev: https://docs.google.com/document/d/1bxYCEbM4Wn2uLl_lgY67a6x5VBWH_ZGdLwG_yDByfT0/edit?pli=1#heading=h.bti4iq4qm4f0
### Adoptium Testing Aqavit Room <!--{% raw %}-->{{< badge prefix="people" text="512" >}}<!--{% endraw %}-->
**[#adoptium-testing-aqavit:matrix.eclipse.org](https://chat.eclipse.org/#/room/#adoptium-testing-aqavit:matrix.eclipse.org)**

Topics relating to testing activities done as part of the activities under the AQAvit project
### BPDM Room <!--{% raw %}-->{{< badge prefix="people" text="65" >}}<!--{% endraw %}-->
**[#tractusx-bpdm:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-bpdm:matrix.eclipse.org)**

Business Partner Data Management, BPDM Components, Value Added Services.
### Country Risk Room <!--{% raw %}-->{{< badge prefix="people" text="29" >}}<!--{% endraw %}-->
**[#tractusx-country-risk:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-country-risk:matrix.eclipse.org)**

Room for Country Risk topics discussion and alignment
### EDC Room <!--{% raw %}-->{{< badge prefix="people" text="200" >}}<!--{% endraw %}-->
**[#tractusx-edc:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-edc:matrix.eclipse.org)**

The Tractus-X EDC provides pre-built control- and data-plane docker images and helm charts of the Eclipse DataSpaceConnector Project.
### EDC Architecture Room <!--{% raw %}-->{{< badge prefix="people" text="89" >}}<!--{% endraw %}-->
**[#tractusx-edc-architecture:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-edc-architecture:matrix.eclipse.org)**

Alignment about Tractus-X EDC roadmap, business requirements, architecture and PRs.
### Eclipse 4diac FORTE Room <!--{% raw %}-->{{< badge prefix="people" text="35" >}}<!--{% endraw %}-->
**[#4diac-forte:matrix.eclipse.org](https://chat.eclipse.org/#/room/#4diac-forte:matrix.eclipse.org)**

Questions and discussions targeting 4diac FORTE.
### Eclipse 4diac IDE Room <!--{% raw %}-->{{< badge prefix="people" text="30" >}}<!--{% endraw %}-->
**[#4diac-ide:matrix.eclipse.org](https://chat.eclipse.org/#/room/#4diac-ide:matrix.eclipse.org)**

Questions and discussions targeting 4diac IDE.
### Eclipse 4diac Town Square Room <!--{% raw %}-->{{< badge prefix="people" text="21" >}}<!--{% endraw %}-->
**[#4diac-townsquare:matrix.eclipse.org](https://chat.eclipse.org/#/room/#4diac-townsquare:matrix.eclipse.org)**

A town square for the Eclipse 4diac project, the place for conversations that do not fit any dedicated Matrix room.
### Eclipse Automotive Room <!--{% raw %}-->{{< badge prefix="people" text="28" >}}<!--{% endraw %}-->
**[#automotive:matrix.eclipse.org](https://chat.eclipse.org/#/room/#automotive:matrix.eclipse.org)**

The Eclipse Automotive Top-Level Project provides a space for open source projects to explore ideas and technologies addressing challenges in the automotive, mobility and transportation domain. It is the common goal of this project to provide tools and composable building blocks to empower the development of solutions for the mobility of the future.
### Eclipse GEMOC Studio Room <!--{% raw %}-->{{< badge prefix="people" text="10" >}}<!--{% endraw %}-->
**[#gemoc-studio:matrix.eclipse.org](https://chat.eclipse.org/#/room/#gemoc-studio:matrix.eclipse.org)**

The Eclipse GEMOC Studio is a language workbench for designing and integrating EMF-based modeling languages. It gives a special focus on behavior integration by providing a framework with a generic interface to plug in different execution engines!
### Eclipse ICE Room <!--{% raw %}-->{{< badge prefix="people" text="3" >}}<!--{% endraw %}-->
**[#ice:matrix.eclipse.org](https://chat.eclipse.org/#/room/#ice:matrix.eclipse.org)**

The Eclipse Integrated Computational Environment (ICE) is a scientific workbench and workflow environment developed to improve the user experience for computational scientists.
### Eclipse JGit Reviews Room <!--{% raw %}-->{{< badge prefix="people" text="10" >}}<!--{% endraw %}-->
**[#technology.jgit.reviews:matrix.eclipse.org](https://chat.eclipse.org/#/room/#technology.jgit.reviews:matrix.eclipse.org)**

Discuss JGit changes
### Eclipse Krazo™ Room <!--{% raw %}-->{{< badge prefix="people" text="4" >}}<!--{% endraw %}-->
**[#ee4j.krazo:matrix.eclipse.org](https://chat.eclipse.org/#/room/#ee4j.krazo:matrix.eclipse.org)**

Eclipse Krazo™ is an implementation of action-based MVC specified by MVC 1.0 (JSR-371). It builds on top of JAX-RS and currently contains support for RESTEasy, Jersey and CXF with a well-defined SPI for other implementations.
### Eclipse LSP4J Builds Room <!--{% raw %}-->{{< badge prefix="people" text="6" >}}<!--{% endraw %}-->
**[#eclipse-lsp4j-builds:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-lsp4j-builds:matrix.eclipse.org)**

Notification Channel for Eclipse LSP4J Build Results
### Eclipse Modeling Workflow Engine Builds Room <!--{% raw %}-->{{< badge prefix="people" text="3" >}}<!--{% endraw %}-->
**[#modeling-emf-mwe-builds:matrix.eclipse.org](https://chat.eclipse.org/#/room/#modeling-emf-mwe-builds:matrix.eclipse.org)**

Eclipse Modeling Workflow Engine Builds
### Eclipse Mylyn Room <!--{% raw %}-->{{< badge prefix="people" text="8" >}}<!--{% endraw %}-->
**[#tools.mylyn:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tools.mylyn:matrix.eclipse.org)**

Eclipse Mylyn is a Task-Focused Interface for Eclipse that reduces information overload and makes multi-tasking easy. The mission of the Mylyn project is to provide: 1. Frameworks and APIs for Eclipse-based task and Application Lifecycle Management (ALM) 2. Exemplary tools for task-focused programming within the Eclipse IDE. 3. Reference implementations for open source ALM tools used by the Eclipse community and for open ALM standards such as OSLC The project is structured into sub-projects, each representing an ALM category and providing common APIs for specific ALM tools. The primary consumers of this project are ALM ISVs and other adopters of Eclipse ALM frameworks.  Please see the project charter for more details. Mylyn makes tasks a first class part of Eclipse, and integrates rich and offline editing for repositories such as Bugzilla, Trac, and JIRA. Once your tasks are integrated, Mylyn monitors your work activity to identify information relevant to the task-at-hand, and uses this task context to focus the Eclipse UI on the interesting information, hide the uninteresting, and automatically find what's related. This puts the information you need to get work done at your fingertips and improves productivity by reducing searching, scrolling, and navigation. By making task context explicit Mylyn also facilitates multitasking, planning, reusing past efforts, and sharing expertise.
### Eclipse Newcomers Room <!--{% raw %}-->{{< badge prefix="people" text="618" >}}<!--{% endraw %}-->
**[#eclipse-newcomers:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-newcomers:matrix.eclipse.org)**

Welcome to the Eclipse Newcomers Room! Feel free to ask questions, seek guidance, and connect with other newcomers and experienced community members.You can also directly connect with some of our existing communities:* [Eclipse IDE](https://chat.eclipse.org/#/room/#eclipse-ide-newcomers:matrix.eclipse.org),* [Adoptium](https://chat.eclipse.org/#/room/#adoptium-general:matrix.eclipse.org), * [ThreadX](https://chat.eclipse.org/#/room/#threadx:matrix.eclipse.org),* [Oniro](https://chat.eclipse.org/#/room/#oniro-project:matrix.eclipse.org),* [Tractus-X](https://chat.eclipse.org/#/room/#tractusx:matrix.eclipse.org),* [Eclipse Projects](https://chat.eclipse.org/#/room/#eclipse-projects:matrix.eclipse.org) for a complete list of Eclipse Project on Chat Service.
### Eclipse Packager Room <!--{% raw %}-->{{< badge prefix="people" text="6" >}}<!--{% endraw %}-->
**[#packager:matrix.eclipse.org](https://chat.eclipse.org/#/room/#packager:matrix.eclipse.org)**

The Eclipse Packager™ project offers a set of core functionality to work with RPM and Debian package files in plain Java. This functionality is offered in simple JAR variant to create your own solutions, or in ready-to-run tools like an Apache Maven plugin.
### Eclipse SUMO™ Room <!--{% raw %}-->{{< badge prefix="people" text="90" >}}<!--{% endraw %}-->
**[#automotive.sumo:matrix.eclipse.org](https://chat.eclipse.org/#/room/#automotive.sumo:matrix.eclipse.org)**

General discussions about the Eclipse SUMO (Simulation of Urban MObility) project
### Eclipse Science General Room <!--{% raw %}-->{{< badge prefix="people" text="2" >}}<!--{% endraw %}-->
**[#science-general:matrix.eclipse.org](https://chat.eclipse.org/#/room/#science-general:matrix.eclipse.org)**

General room for engaging on Eclipse Science Projects and Science Top-Level Project work.
### Eclipse Semantic Modeling Framework Room <!--{% raw %}-->{{< badge prefix="people" text="21" >}}<!--{% endraw %}-->
**[#eclipse-semantic-modeling-framework:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-semantic-modeling-framework:matrix.eclipse.org)**

Chat about the Eclipse Semantic Modeling Framework and its Semantic Aspect Meta Model, and working with Aspect Models (with or without Aspect Model Editor).Please note that this is a chat. For discussion of involved topics in a thread, we recommend the forum https://www.eclipse.org/forums/index.php/f/617/.See also https://github.com/eclipse-esmf, https://projects.eclipse.org/projects/dt.esmf
### Eclipse Theia Room <!--{% raw %}-->{{< badge prefix="people" text="19" >}}<!--{% endraw %}-->
**[#ecd.theia:matrix.eclipse.org](https://chat.eclipse.org/#/room/#ecd.theia:matrix.eclipse.org)**

Eclipse Theia™ is an extensible platform to develop full-fledged, multi-language, cloud & desktop IDE-like products with state-of-the-art web technologies.
### Eclipse Velocitas Room <!--{% raw %}-->{{< badge prefix="people" text="19" >}}<!--{% endraw %}-->
**[#velocitas:matrix.eclipse.org](https://chat.eclipse.org/#/room/#velocitas:matrix.eclipse.org)**

Eclipse Velocitas provides an end-to-end, scalable, modular and open source development toolchain for creating containerized and non-containerized in-vehicle applications.
### Eclipse Xpanse Room <!--{% raw %}-->{{< badge prefix="people" text="18" >}}<!--{% endraw %}-->
**[#technology.xpanse:matrix.eclipse.org](https://chat.eclipse.org/#/room/#technology.xpanse:matrix.eclipse.org)**

Place for all conversations related to eclipse-xpanse.
### Eclipse Xpect Builds Room <!--{% raw %}-->{{< badge prefix="people" text="3" >}}<!--{% endraw %}-->
**[#modeling-xpect-build:matrix.eclipse.org](https://chat.eclipse.org/#/room/#modeling-xpect-build:matrix.eclipse.org)**

Notification Channel for Eclipse Xpect™ Builds
### Eclipse Xtext Room <!--{% raw %}-->{{< badge prefix="people" text="15" >}}<!--{% endraw %}-->
**[#xtext:matrix.eclipse.org](https://chat.eclipse.org/#/room/#xtext:matrix.eclipse.org)**

Eclipse Xtext
### Jakarta EE Working Group Participant Members Room <!--{% raw %}-->{{< badge prefix="people" text="12" >}}<!--{% endraw %}-->
**[#jakartaee-wg-participant-members:matrix.eclipse.org](https://chat.eclipse.org/#/room/#jakartaee-wg-participant-members:matrix.eclipse.org)**

Channel for participant members interested to be involved and having on-going conversations
### Jakarta MVC™ Room <!--{% raw %}-->{{< badge prefix="people" text="5" >}}<!--{% endraw %}-->
**[#ee4j.mvc:matrix.eclipse.org](https://chat.eclipse.org/#/room/#ee4j.mvc:matrix.eclipse.org)**

Jakarta Model-View-Controller, or Jakarta MVC™ for short, is a common pattern in Web frameworks where it is used predominantly to build HTML applications. The model refers to the application’s data, the view to the application’s data presentation and the controller to the part of the system responsible for managing input, updating models and producing output. Web UI frameworks can be categorized as action-based or component-based. In an action-based framework, HTTP requests are routed to controllers where they are turned into actions by application code; in a component-based framework, HTTP requests are grouped and typically handled by framework components with little or no interaction from application code. In other words, in a component-based framework, the majority of the controller logic is provided by the framework instead of the application.
### Jakarta RESTful Web Services™ Room <!--{% raw %}-->{{< badge prefix="people" text="13" >}}<!--{% endraw %}-->
**[#ee4j.rest:matrix.eclipse.org](https://chat.eclipse.org/#/room/#ee4j.rest:matrix.eclipse.org)**

Jakarta RESTful Web Services™ provides a specification document, TCK and foundational API to develop web services following the Representational State Transfer (REST) architectural pattern. JAX-RS: Java API for RESTful Web Services (JAX-RS) is a Java programming language API spec that provides support in creating web services according to the Representational State Transfer (REST) architectural pattern.
### Kiso testing committer Room <!--{% raw %}-->{{< badge prefix="people" text="8" >}}<!--{% endraw %}-->
**[#kiso-testing-committer:matrix.eclipse.org](https://chat.eclipse.org/#/room/#kiso-testing-committer:matrix.eclipse.org)**

Strategy discussions regarding pykiso (roadmap, next features, next PRs to merge)
### Kiso testing contributor Room <!--{% raw %}-->{{< badge prefix="people" text="7" >}}<!--{% endraw %}-->
**[#kiso-testing-contributor:matrix.eclipse.org](https://chat.eclipse.org/#/room/#kiso-testing-contributor:matrix.eclipse.org)**

Technical discussions regarding pykiso and the different contributions
### Oniro Chromium Room <!--{% raw %}-->{{< badge prefix="people" text="22" >}}<!--{% endraw %}-->
**[#oniro-chromium:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro-chromium:matrix.eclipse.org)**

Development of Chromium-related projects for Oniro.
### Oniro IDE Room <!--{% raw %}-->{{< badge prefix="people" text="40" >}}<!--{% endraw %}-->
**[#oniro-ide:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro-ide:matrix.eclipse.org)**

Development of IDE for Oniro.
### Oniro Marketing Room <!--{% raw %}-->{{< badge prefix="people" text="19" >}}<!--{% endraw %}-->
**[#oniro-marketing:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro-marketing:matrix.eclipse.org)**

Oniro Marketing channel is the space to discuss and coordinate the execution of the activities defined by the WG Marketing committee.
### Oniro Project Room <!--{% raw %}-->{{< badge prefix="people" text="76" >}}<!--{% endraw %}-->
**[#oniro-project:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro-project:matrix.eclipse.org)**

A town square for Oniro Project, the place for conversations that do not fit any dedicated Matrix room.
### Oniro for OpenHarmony Room <!--{% raw %}-->{{< badge prefix="people" text="55" >}}<!--{% endraw %}-->
**[#oniro4openharmony:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro4openharmony:matrix.eclipse.org)**

Eclipse Oniro for OpenHarmony
### SSI Room <!--{% raw %}-->{{< badge prefix="people" text="122" >}}<!--{% endraw %}-->
**[#tractusx-ssi:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-ssi:matrix.eclipse.org)**

Eclipse Tractus-X™ component SSI
### Security Room <!--{% raw %}-->{{< badge prefix="people" text="54" >}}<!--{% endraw %}-->
**[#tractusx-security:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-security:matrix.eclipse.org)**

Room for security topics, discussions and alignment
### Trace-X Room <!--{% raw %}-->{{< badge prefix="people" text="51" >}}<!--{% endraw %}-->
**[#tractusx-trace-x:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-trace-x:matrix.eclipse.org)**

Trace-X is a system for tracking parts along the supply chain.
### Tractus-X Room <!--{% raw %}-->{{< badge prefix="people" text="225" >}}<!--{% endraw %}-->
**[#tractusx:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx:matrix.eclipse.org)**

Eclipse Tractus-X™ aims at the development of systems in support of the Catena-X data space, with focus on PLM / quality, resiliency, sustainability, and shared network services.
### Umbrella chart Room <!--{% raw %}-->{{< badge prefix="people" text="69" >}}<!--{% endraw %}-->
**[#tractusx-umbrella-chart:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-umbrella-chart:matrix.eclipse.org)**

Room for discussion and collaboration for the overarching Umbrella chart https://github.com/eclipse-tractusx/tractus-x-umbrellaGitHub Project: https://github.com/orgs/eclipse-tractusx/projects/80/views/1
### Xtext Builds Room <!--{% raw %}-->{{< badge prefix="people" text="5" >}}<!--{% endraw %}-->
**[#xtext-releng:matrix.eclipse.org](https://chat.eclipse.org/#/room/#xtext-releng:matrix.eclipse.org)**

Notification Channel for Xtext Build Results
_________________
## **Eclipse SW360 Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="4" >}} {{< badge prefix=people text="12" >}}<!--{% endraw %}-->
**[#eclipse-sw360:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-sw360:matrix.eclipse.org)**

This is the space for Eclipse SW360 project and its related rooms.
### Eclipse SW360 Dev Room Room <!--{% raw %}-->{{< badge prefix="people" text="6" >}}<!--{% endraw %}-->
**[#technology.sw360-dev:matrix.eclipse.org](https://chat.eclipse.org/#/room/#technology.sw360-dev:matrix.eclipse.org)**

Eclipse SW360 project Dev Room. Place for developer specific conversations.
### Eclipse SW360 General Room <!--{% raw %}-->{{< badge prefix="people" text="54" >}}<!--{% endraw %}-->
**[#technology.sw360-general:matrix.eclipse.org](https://chat.eclipse.org/#/room/#technology.sw360-general:matrix.eclipse.org)**

Place for all conversations related to eclipse-sw360.
### Eclipse SW360 Steering Committee Room <!--{% raw %}-->{{< badge prefix="people" text="11" >}}<!--{% endraw %}-->
**[#technology.sw360-steeringcommittee:matrix.eclipse.org](https://chat.eclipse.org/#/room/#technology.sw360-steeringcommittee:matrix.eclipse.org)**

Eclipse SW360 project Steering Committee discussions
### Eclipse SW360 UI Room <!--{% raw %}-->{{< badge prefix="people" text="10" >}}<!--{% endraw %}-->
**[#technology.sw360-ui:matrix.eclipse.org](https://chat.eclipse.org/#/room/#technology.sw360-ui:matrix.eclipse.org)**

Discussions regarding new front-end project of SW360 UI
_________________
## **Eclipse Science Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="2" >}} {{< badge prefix=people text="2" >}}<!--{% endraw %}-->
**[#science:matrix.eclipse.org](https://chat.eclipse.org/#/room/#science:matrix.eclipse.org)**

The Science top-level project provides a central clearinghouse for collaborative development efforts to create software for scientific research and development.
### Eclipse ICE Room <!--{% raw %}-->{{< badge prefix="people" text="3" >}}<!--{% endraw %}-->
**[#ice:matrix.eclipse.org](https://chat.eclipse.org/#/room/#ice:matrix.eclipse.org)**

The Eclipse Integrated Computational Environment (ICE) is a scientific workbench and workflow environment developed to improve the user experience for computational scientists.
### Eclipse Science General Room <!--{% raw %}-->{{< badge prefix="people" text="2" >}}<!--{% endraw %}-->
**[#science-general:matrix.eclipse.org](https://chat.eclipse.org/#/room/#science-general:matrix.eclipse.org)**

General room for engaging on Eclipse Science Projects and Science Top-Level Project work.
_________________
## **Eclipse ThreadX Space Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="7" >}} {{< badge prefix=people text="20" >}}<!--{% endraw %}-->
**[#eclipse-threadx:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-threadx:matrix.eclipse.org)**

Space to discuss Eclipse ThreadX® topics.
### Eclipse ThreadX Room <!--{% raw %}-->{{< badge prefix="people" text="17" >}}<!--{% endraw %}-->
**[#threadx:matrix.eclipse.org](https://chat.eclipse.org/#/room/#threadx:matrix.eclipse.org)**

Eclipse ThreadX is a real-time operating system (RTOS) for Internet of Things (IoT) and edge devices powered by microcontroller units (MCUs). General conversations about ThreadX go here, as well as exchanges about the ThreadX kernel, including ThreadX SMP.
### Eclipse ThreadX FileX Room <!--{% raw %}-->{{< badge prefix="people" text="10" >}}<!--{% endraw %}-->
**[#threadx.filex:matrix.eclipse.org](https://chat.eclipse.org/#/room/#threadx.filex:matrix.eclipse.org)**

The FileX embedded file system is an industrial-grade implementation of the Microsoft FAT file formats, designed specifically for deeply embedded, real-time, and IoT applications. FileX supports FAT12, FAT16, and FAT32.
### Eclipse ThreadX GUIX Room <!--{% raw %}-->{{< badge prefix="people" text="5" >}}<!--{% endraw %}-->
**[#threadx.guix:matrix.eclipse.org](https://chat.eclipse.org/#/room/#threadx.guix:matrix.eclipse.org)**

GUIX is an advanced GUI solution designed specifically for deeply embedded, real-time, and IoT applications. The ThreadX team also provides a full-featured WYSIWYG desktop design tool named GUIX Studio, which allows developers to design their GUI on their workstations and generate GUIX embedded GUI code that can then be exported to the target.
### Eclipse ThreadX LevelX Room <!--{% raw %}-->{{< badge prefix="people" text="6" >}}<!--{% endraw %}-->
**[#threadx.levelx:matrix.eclipse.org](https://chat.eclipse.org/#/room/#threadx.levelx:matrix.eclipse.org)**

LevelX provides NAND and NOR flash wear leveling facilities to embedded applications. It is generally used in conjunction with FileX.
### Eclipse ThreadX NetX Duo Room <!--{% raw %}-->{{< badge prefix="people" text="7" >}}<!--{% endraw %}-->
**[#threadx.netx:matrix.eclipse.org](https://chat.eclipse.org/#/room/#threadx.netx:matrix.eclipse.org)**

NetX Duo is a lightweight dual IPv4 and IPv6 TCP/IP network stack designed specifically for highly constrained devices. It supports TLS/DTLS for encrypted communications.
### Eclipse ThreadX TraceX Room <!--{% raw %}-->{{< badge prefix="people" text="7" >}}<!--{% endraw %}-->
**[#threadx.tracex:matrix.eclipse.org](https://chat.eclipse.org/#/room/#threadx.tracex:matrix.eclipse.org)**

TraceX is a host-based analysis tool that provides developers with a graphical view of real-time system events and enables them to visualize and better understand the behaviour of their real-time systems.
### Eclipse ThreadX USBX Room <!--{% raw %}-->{{< badge prefix="people" text="7" >}}<!--{% endraw %}-->
**[#threadx.usbx:matrix.eclipse.org](https://chat.eclipse.org/#/room/#threadx.usbx:matrix.eclipse.org)**

USBX is a high-performance USB host, device, and on-the-go (OTG) embedded stack.
_________________
## **Eclipse Xtext Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="2" >}} {{< badge prefix=people text="4" >}}<!--{% endraw %}-->
**[#eclipse-xtext:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-xtext:matrix.eclipse.org)**

Eclipse Xtext™ is a framework for development of programming languages and domain specific languages. It covers all aspects of a complete language infrastructure, from parsers, over linker, compiler or interpreter to fully-blown top-notch Eclipse IDE integration. It comes with good defaults for all these aspects and at the same time every single aspect can be tailored to your needs.
### Eclipse Xtext Room <!--{% raw %}-->{{< badge prefix="people" text="15" >}}<!--{% endraw %}-->
**[#xtext:matrix.eclipse.org](https://chat.eclipse.org/#/room/#xtext:matrix.eclipse.org)**

Eclipse Xtext
### Xtext Builds Room <!--{% raw %}-->{{< badge prefix="people" text="5" >}}<!--{% endraw %}-->
**[#xtext-releng:matrix.eclipse.org](https://chat.eclipse.org/#/room/#xtext-releng:matrix.eclipse.org)**

Notification Channel for Xtext Build Results
_________________
## **Jakarta EE Working Group Space Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="1" >}} {{< badge prefix=people text="4" >}}<!--{% endraw %}-->
**[#jakartaee-wg:matrix.eclipse.org](https://chat.eclipse.org/#/room/#jakartaee-wg:matrix.eclipse.org)**

Space to discuss Jakarta EE Working topics
### Jakarta EE Working Group Participant Members Room <!--{% raw %}-->{{< badge prefix="people" text="12" >}}<!--{% endraw %}-->
**[#jakartaee-wg-participant-members:matrix.eclipse.org](https://chat.eclipse.org/#/room/#jakartaee-wg-participant-members:matrix.eclipse.org)**

Channel for participant members interested to be involved and having on-going conversations
_________________
## **Jakarta RESTful Web Services™ Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="1" >}} {{< badge prefix=people text="5" >}}<!--{% endraw %}-->
**[#jaxrs:matrix.eclipse.org](https://chat.eclipse.org/#/room/#jaxrs:matrix.eclipse.org)**

Jakarta RESTful Web Services™ provides a specification document, TCK and foundational API to develop web services following the Representational State Transfer (REST) architectural pattern. JAX-RS: Java API for RESTful Web Services (JAX-RS) is a Java programming language API spec that provides support in creating web services according to the Representational State Transfer (REST) architectural pattern.
### Jakarta RESTful Web Services™ Room <!--{% raw %}-->{{< badge prefix="people" text="13" >}}<!--{% endraw %}-->
**[#ee4j.rest:matrix.eclipse.org](https://chat.eclipse.org/#/room/#ee4j.rest:matrix.eclipse.org)**

Jakarta RESTful Web Services™ provides a specification document, TCK and foundational API to develop web services following the Representational State Transfer (REST) architectural pattern. JAX-RS: Java API for RESTful Web Services (JAX-RS) is a Java programming language API spec that provides support in creating web services according to the Representational State Transfer (REST) architectural pattern.
_________________
## **Kiso testing Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="2" >}} {{< badge prefix=people text="7" >}}<!--{% endraw %}-->
**[#kiso-testing:matrix.eclipse.org](https://chat.eclipse.org/#/room/#kiso-testing:matrix.eclipse.org)**

Space to discuss pykiso topics
### Kiso testing committer Room <!--{% raw %}-->{{< badge prefix="people" text="8" >}}<!--{% endraw %}-->
**[#kiso-testing-committer:matrix.eclipse.org](https://chat.eclipse.org/#/room/#kiso-testing-committer:matrix.eclipse.org)**

Strategy discussions regarding pykiso (roadmap, next features, next PRs to merge)
### Kiso testing contributor Room <!--{% raw %}-->{{< badge prefix="people" text="7" >}}<!--{% endraw %}-->
**[#kiso-testing-contributor:matrix.eclipse.org](https://chat.eclipse.org/#/room/#kiso-testing-contributor:matrix.eclipse.org)**

Technical discussions regarding pykiso and the different contributions
_________________
## **Oniro Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="5" >}} {{< badge prefix=people text="62" >}}<!--{% endraw %}-->
**[#oniro:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro:matrix.eclipse.org)**

The mission of the Eclipse Oniro is the design, development, production and maintenance of an open source software platform, having an operating system, an ADK/SDK, standard APIs and basic applications, like UI, as core elements, targeting different industries thanks to a next-generation multi-kernel architecture, that simplifies the existing landscape of complex systems, and its deployment across a wide range of devices.
### Oniro Chromium Room <!--{% raw %}-->{{< badge prefix="people" text="22" >}}<!--{% endraw %}-->
**[#oniro-chromium:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro-chromium:matrix.eclipse.org)**

Development of Chromium-related projects for Oniro.
### Oniro IDE Room <!--{% raw %}-->{{< badge prefix="people" text="40" >}}<!--{% endraw %}-->
**[#oniro-ide:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro-ide:matrix.eclipse.org)**

Development of IDE for Oniro.
### Oniro Marketing Room <!--{% raw %}-->{{< badge prefix="people" text="19" >}}<!--{% endraw %}-->
**[#oniro-marketing:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro-marketing:matrix.eclipse.org)**

Oniro Marketing channel is the space to discuss and coordinate the execution of the activities defined by the WG Marketing committee.
### Oniro Project Room <!--{% raw %}-->{{< badge prefix="people" text="76" >}}<!--{% endraw %}-->
**[#oniro-project:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro-project:matrix.eclipse.org)**

A town square for Oniro Project, the place for conversations that do not fit any dedicated Matrix room.
### Oniro for OpenHarmony Room <!--{% raw %}-->{{< badge prefix="people" text="55" >}}<!--{% endraw %}-->
**[#oniro4openharmony:matrix.eclipse.org](https://chat.eclipse.org/#/room/#oniro4openharmony:matrix.eclipse.org)**

Eclipse Oniro for OpenHarmony
_________________
## **Open Regulatory Compliance Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="3" >}} {{< badge prefix=people text="62" >}}<!--{% endraw %}-->
**[#open-regulatory-compliance:matrix.eclipse.org](https://chat.eclipse.org/#/room/#open-regulatory-compliance:matrix.eclipse.org)**

The Eclipse Open Regulatory Compliance Working Group is being created to meet the needs of new government regulations on the software industry which will impact open source communities.  This working group is intended to bring together key stakeholders from industry, small and medium enterprise (SME), research, and open source foundations to work with government in forging specifications that will enable industry to continue to leverage open source through the software supply chain to meet those regulatory requirements, and in turn will enable the open source projects to better meet industry's needs in this regard.
### CRA Consultations Room <!--{% raw %}-->{{< badge prefix="people" text="33" >}}<!--{% endraw %}-->
**[#open-regulatory-compliance-cra-consultations:matrix.eclipse.org](https://chat.eclipse.org/#/room/#open-regulatory-compliance-cra-consultations:matrix.eclipse.org)**

Backchannel for ORC WG coordination during CRA consultations on important and critical product definitions
### General Room <!--{% raw %}-->{{< badge prefix="people" text="56" >}}<!--{% endraw %}-->
**[#open-regulatory-compliance-general:matrix.eclipse.org](https://chat.eclipse.org/#/room/#open-regulatory-compliance-general:matrix.eclipse.org)**

General Discussions: * [Main GitLab organisation](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg) - links to all of the key resources for the ORC WG.* [CRA-specific GitLab repository](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics#eu-cyber-resilience-act-cra-discussion-topics) - this is where all of the CRA-related work is taking place.* [Mailing list archive](https://www.eclipse.org/lists/open-regulatory-compliance/maillist.html) - the archive for this mailing list.* [Community Calendar](https://calendar.google.com/calendar/u/0/embed?src=c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea@group.calendar.google.com) ([iCal format](https://calendar.google.com/calendar/ical/c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea%40group.calendar.google.com/public/basic.ics)) - find all of the group’s meetings and calls.* [Weekly office hours on Zoom](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/gitlab-profile/-/blob/main/README.md#office-hours) - every Tuesday from 4pm to 5pm CEST (14:00 - 15:00 UTC), informal conversation to get up to speed.
### Specification Project Proposals Room <!--{% raw %}-->{{< badge prefix="people" text="40" >}}<!--{% endraw %}-->
**[#open-regulatory-compliance-specification-project-proposals:matrix.eclipse.org](https://chat.eclipse.org/#/room/#open-regulatory-compliance-specification-project-proposals:matrix.eclipse.org)**

Specificaton Project Proposals Dicussions
_________________
## **Tractus-X Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="15" >}} {{< badge prefix=people text="128" >}}<!--{% endraw %}-->
**[#automotive.tractusx:matrix.eclipse.org](https://chat.eclipse.org/#/room/#automotive.tractusx:matrix.eclipse.org)**

Eclipse Tractus-X™ space
### BPDM Room <!--{% raw %}-->{{< badge prefix="people" text="65" >}}<!--{% endraw %}-->
**[#tractusx-bpdm:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-bpdm:matrix.eclipse.org)**

Business Partner Data Management, BPDM Components, Value Added Services.
### Committers Room <!--{% raw %}-->{{< badge prefix="people" text="60" >}}<!--{% endraw %}-->
**[#tractusx-committers:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-committers:matrix.eclipse.org)**

A room for all the committers from the Tractus-X Project. The topics discussed in this room will be defined in the [Open hour meeting](https://eclipse-tractusx.github.io/community/open-meetings) for Eclipse Tractus-X committers.
### Country Risk Room <!--{% raw %}-->{{< badge prefix="people" text="29" >}}<!--{% endraw %}-->
**[#tractusx-country-risk:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-country-risk:matrix.eclipse.org)**

Room for Country Risk topics discussion and alignment
### EDC Room <!--{% raw %}-->{{< badge prefix="people" text="200" >}}<!--{% endraw %}-->
**[#tractusx-edc:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-edc:matrix.eclipse.org)**

The Tractus-X EDC provides pre-built control- and data-plane docker images and helm charts of the Eclipse DataSpaceConnector Project.
### EDC Architecture Room <!--{% raw %}-->{{< badge prefix="people" text="89" >}}<!--{% endraw %}-->
**[#tractusx-edc-architecture:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-edc-architecture:matrix.eclipse.org)**

Alignment about Tractus-X EDC roadmap, business requirements, architecture and PRs.
### Industry Core Hub Room <!--{% raw %}-->{{< badge prefix="people" text="26" >}}<!--{% endraw %}-->
**[#tractusx-industry-core-hub:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-industry-core-hub:matrix.eclipse.org)**

Room for development/alignment of the Industry Core Hub and the python Tractus-X SDK.
### Item Relationship Service Room <!--{% raw %}-->{{< badge prefix="people" text="33" >}}<!--{% endraw %}-->
**[#tractusx-irs:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-irs:matrix.eclipse.org)**

The vision for the Item Relationship Service is to provide an easy access endpoint for complex distributed digital twins across Catena-X members. It abstracts the access from separated digital twins towards a connected data chain of twins and provides those. It enables to apply business logic on complex distributed digital twins across company borders.
### Portal Room <!--{% raw %}-->{{< badge prefix="people" text="75" >}}<!--{% endraw %}-->
**[#tractusx-portal:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-portal:matrix.eclipse.org)**

The Portal and Marketplace is the entry point for all activities in the automotive dataspace. This room focuses on Portal and Marketplace related topics as well as on Identity and Access Management.Open Meeting every Thursday: https://eclipse-tractusx.github.io/community/open-meetings#Portal%20-%20Open%20Meeting
### Release Management Room <!--{% raw %}-->{{< badge prefix="people" text="25" >}}<!--{% endraw %}-->
**[#tractusx-release-planning:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-release-planning:matrix.eclipse.org)**

Room to inform and collaboration during Eclipse Tractus-X Release Planning.
### SSI Room <!--{% raw %}-->{{< badge prefix="people" text="122" >}}<!--{% endraw %}-->
**[#tractusx-ssi:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-ssi:matrix.eclipse.org)**

Eclipse Tractus-X™ component SSI
### Security Room <!--{% raw %}-->{{< badge prefix="people" text="54" >}}<!--{% endraw %}-->
**[#tractusx-security:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-security:matrix.eclipse.org)**

Room for security topics, discussions and alignment
### Test Management Room <!--{% raw %}-->{{< badge prefix="people" text="13" >}}<!--{% endraw %}-->
**[#tractusx-test-management:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-test-management:matrix.eclipse.org)**

Room to inform and collaborate during test phases.
### Trace-X Room <!--{% raw %}-->{{< badge prefix="people" text="51" >}}<!--{% endraw %}-->
**[#tractusx-trace-x:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-trace-x:matrix.eclipse.org)**

Trace-X is a system for tracking parts along the supply chain.
### Tractus-X Room <!--{% raw %}-->{{< badge prefix="people" text="225" >}}<!--{% endraw %}-->
**[#tractusx:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx:matrix.eclipse.org)**

Eclipse Tractus-X™ aims at the development of systems in support of the Catena-X data space, with focus on PLM / quality, resiliency, sustainability, and shared network services.
### Umbrella chart Room <!--{% raw %}-->{{< badge prefix="people" text="69" >}}<!--{% endraw %}-->
**[#tractusx-umbrella-chart:matrix.eclipse.org](https://chat.eclipse.org/#/room/#tractusx-umbrella-chart:matrix.eclipse.org)**

Room for discussion and collaboration for the overarching Umbrella chart https://github.com/eclipse-tractusx/tractus-x-umbrellaGitHub Project: https://github.com/orgs/eclipse-tractusx/projects/80/views/1
_________________
## **Xpanse Space**  <!--{% raw %}-->{{< badge theme=blue prefix=room text="1" >}} {{< badge prefix=people text="18" >}}<!--{% endraw %}-->
**[#eclipse-xpanse:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse-xpanse:matrix.eclipse.org)**

xpanse is here to make native cloud services configurable and portable.
### Eclipse Xpanse Room <!--{% raw %}-->{{< badge prefix="people" text="18" >}}<!--{% endraw %}-->
**[#technology.xpanse:matrix.eclipse.org](https://chat.eclipse.org/#/room/#technology.xpanse:matrix.eclipse.org)**

Place for all conversations related to eclipse-xpanse.
